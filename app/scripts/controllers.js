  'use strict';

angular.module('confusionApp')

        .controller('MenuController', ['$scope', 'menuFactory', function($scope, menuFactory) {
            
            $scope.tab = 1;
            $scope.filtText = '';
            $scope.showDetails = false;
            
            $scope.showMenu = false;
            $scope.message = "Loading ...";
            $scope.dishes= menuFactory.getDishes().query(
            function(response) {
                    
                        $scope.dishes = response;
                        $scope.showMenu = true;
                        
                }, 
            function(response){
                $scope.message = "Error: "+response.status + " " + response.statusText;
            }
            );
            
//            menuFactory.getDishes()
//                .then(
//                    function(response) {
//                    
//                        $scope.dishes = response.data;
//                        $scope.showMenu = true;
//                        
//                }, 
//            function(response){
//                $scope.message = "Error: "+response.status + " " + response.statusText;
//            }
//            
//            );

                        
            $scope.select = function(setTab) {
                $scope.tab = setTab;
                
                if (setTab === 2) {
                    $scope.filtText = "appetizer";
                }
                else if (setTab === 3) {
                    $scope.filtText = "mains";
                }
                else if (setTab === 4) {
                    $scope.filtText = "dessert";
                }
                else {
                    $scope.filtText = "";
                }
            };

            $scope.isSelected = function (checkTab) {
                return ($scope.tab === checkTab);
            };
    
            $scope.toggleDetails = function() {
                $scope.showDetails = !$scope.showDetails;
            };
        }])

        
        .controller('FeedbackController', ['$scope', 'feedbackFactory', function($scope, feedbackFactory) {
            
            $scope.feedback = {firstname:"", lastname:"", telcode:"", telnum:"", emailid:"", contactyesorno:"", modeofcontact:"", feedback:"" };
           
            $scope.sendFeedback = function() {
                
                    feedbackFactory.getFeedback().save($scope.feedback);
                    console.log($scope.feedback);
                    $scope.feedbackForm.$setPristine();
                    $scope.feedback = {firstname:"", lastname:"", telcode:"", telnum:"", emailid:"", contactyesorno:"", modeofcontact:"", feedback:"" };
                    
                };
            }
            
        ])

        .controller('DishDetailController', ['$scope', '$stateParams', 'menuFactory', function($scope, $stateParams, menuFactory) {
            
            $scope.dish = {};
            
            $scope.showDish=false;
            
            $scope.message= "Loading ...";
            
            $scope.dish = menuFactory.getDishes().get({id:parseInt($stateParams.id,10)})
            .$promise.then(
            
            
                function(response){
                    
                    $scope.dish = response;
                    $scope.showDish=true;
                },
            function(response){
                
                $scope.message = "Error: "+response.status + " " + response.statusText;                
                
            }
                
            
            
            );
            
//            menuFactory.getDish(parseInt($stateParams.id,10))
//            .then( 
//                function(response){
//                    
//                    $scope.dish = response.data;
//                    $scope.showDish=true;
//                },
//            function(response){
//                
//                $scope.message = "Error: "+response.status + " " + response.statusText;                
//                
//            });
            
           
            
        }])

        .controller('DishCommentController', ['$scope', 'menuFactory', function($scope, menuFactory) {
            
            //Step 1: Create a JavaScript object to hold the comment from the form
              
              $scope.commentback = { rating:"5",
                                     comment:"",
                                   author:"",
                                   date: ""
                                   };
              
               
              
              $scope.submitComment = function () {
                
                //Step 2: This is how you record the date
                var commentDate = new Date().toISOString();
                  
                $scope.commentback.date = commentDate;
            
                // Step 3: Push your comment into the dish's comment array
                $scope.dish.comments.push($scope.commentback);
                  
                  
                menuFactory.getDishes().update({id:$scope.dish.id},$scope.dish);
                                
                //Step 4: reset your form to pristine
                $scope.commentForm.$setPristine();
                  
                  
                //Step 5: reset your JavaScript object that holds your comment
              $scope.commentback = { rating:"5",
                                     comment:"",
                                   author:"",
                                   date: ""
                                   };
              
              
           };
        }])


         // implement the IndexController and About Controller here
    
        .controller('IndexController', ['$scope', 'menuFactory', 'corporateFactory', function($scope, menuFactory, corporateFactory){
            
                $scope.promotion = {};
                $scope.showPromotion = false;
                $scope.message="Loading ...";
            
            
             $scope.promotion = menuFactory.getPromotion().get({id:0})
            .$promise.then(
            
            
                function(response){
                                $scope.promotion = response;
                                $scope.showPromotion = true;
                }, 
                function(response){
                    
                    $scope.message = "Error: "+response.status + " " + response.statusText;
                    
                }
             
             );
            
            
            $scope.execChef = {};
            $scope.showExecChef = false;
            $scope.message="Loading ...";
            
            $scope.execChef = corporateFactory.getLeaders().get({id:3})
            .$promise.then(
            
            
                function(response){
                                $scope.execChef = response;
                                $scope.showExecChef = true;
                }, 
                function(response){
                    
                    $scope.message = "Error: "+response.status + " " + response.statusText;
                    
                }
            
            
            
            );
            
            
            $scope.dish = {};
            $scope.showDish = false;
            $scope.message="Loading ...";
            
            $scope.dish = menuFactory.getDishes().get({id:0})
            .$promise.then(
            
            
                function(response){
                                $scope.dish = response;
                                $scope.showDish = true;
                }, 
                function(response){
                    
                    $scope.message = "Error: "+response.status + " " + response.statusText;
                    
                }
            
            
            
            );
            
//            menuFactory.getDish(0)
//            .then(
//                function(response){
//                                $scope.dish = response.data;
//                                $scope.showDish = true;
//                }, 
//                function(response){
//                    
//                    $scope.message = "Error: "+response.status + " " + response.statusText;
//                    
//                });
            
            
        }])

        
        .controller('AboutController', ['$scope', 'corporateFactory', function($scope, corporateFactory){
            
            $scope.showLeaders = false;
            $scope.leaders = {};
            $scope.message="Loading ...";
            
            $scope.leaders = corporateFactory.getLeaders().query(
            function(response) {
                    
                        $scope.leaders = response;
                        $scope.showLeaders = true;
                        
                }, 
            function(response){
                $scope.message = "Error: "+response.status + " " + response.statusText;
            }
            );
            
            
        }])


;
